## Usage:
## cd slate-dev
## source ../install-scripts/slate-saturn-gtx1060-v1.sh 
echo "usage:"
echo "   source ../install-scripts/slate-saturn-gtx1060-v1.sh"
echo

module purge
module load gcc/8.3.0
module load cuda/11.6.0
module load intel-oneapi-mkl/2022
module load intel-oneapi-mpi/2021
export LIBRARY_PATH=$CUDADIR/lib64:$LIBRARY_PATH
export CPATH=$CUDADIR/include:$CPATH

git submodule update --init --recursive

cat > make.inc << END
CXX       = mpicxx
FC        = mpif90
CXXFLAGS  = -g -Werror -DSLATE_NO_HIP
blas      = mkl
cuda_arch = pascal
# openmp=1 by default
hip       = 0

# print time in fortran code
LIBS += -lgfortran
END

echo "========================================"
make distclean
echo "========================================"
ls -R
echo "========================================"
time make -j20 test
echo "========================================"
ldd test/tester

## Comment out to run tests
# cd test
# ./run_tests.py --ref n --xml ../report.xml
